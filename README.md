# Maratona Full Cycle 4.0

[![Maratona Full Cycle 4.0](http://maratona.fullcycle.com.br/static/site/img/logo-fullcycle.png)](http://maratona.fullcycle.com.br/)

Projetos de resposta dos desafios da [Maratona Full Cycle](http://maratona.fullcycle.com.br/) utilizando as tecnologias:

  - Docker
  - Kubernetes
  - Keycloack 
  - Github (CI/CD) 
  - Node.js 
  - Nest.js
